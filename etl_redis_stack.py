#!./venv/bin/python

import sys
import argparse
import json

import redis

parser = argparse.ArgumentParser()
parser.add_argument("--db", help="Redis database number", type=int)
parser.add_argument("--username", help="Redis username")
parser.add_argument("--password", help="Redis password")
args = parser.parse_args()

redis_database = args.db if args.db != None else 0
redis_username = args.username if args.username != None else 'default'
redis_password = args.password if args.password != None else ''

r = redis.Redis(db=redis_database, username=redis_username, password=redis_password)

def insert_to_redis_stack(data):
	temp_json = json.loads(data)
	if r.json().set(('collaborators_' + temp_json['_airbyte_ab_id']), '$', temp_json['_airbyte_data']):
		print(temp_json['_airbyte_ab_id'], "key inserted")
	else:
		print("Something went wrong!")

def main():
	print("redis_stack etl")
	print("database: ", redis_database)
	while True:
		line = sys.stdin.readline()
		if not line:
			print("EOF reached")
			break;
		else:
			insert_to_redis_stack(line)

if __name__=="__main__":
	main()